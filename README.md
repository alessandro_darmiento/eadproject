# README #

### Descrizione ###
L’applicazione Android permette di modificare la lunghezza di una traccia audio senza modificarne l’altezza (pitch). L’applicazione è stata sviluppata secondo le specifiche di progetto: nella prima finestra si può aprire o registrare un file wav pcm 16bit; nella finestra centrale si sceglie il parametro di scala, che serve all’applicazione per elaborare il file secondo l’algoritmo WSOLA implementato; nell’ultima finestra è possibile ascoltare il file elaborato e salvato nella memoria del dispositivo.

### Per avviare l’applicazione si consiglia di seguire la seguente procedura: ###
* Download Android Studio
* New project from VCS -> git -> inserire il Link Git ricevuto nella mail dopo il colloquio di lunedì 09/01/2017. In alternativa importare il progetto dal file .zip ricevuto dal portale della didattica.
* Collegare un dispositivo Android e lanciare l’app, scegliendo tra le voci disponibili il dispositivo appena collegato (attenzione! bisogna confermare sul dispositivo in uso appena collegato il messaggio che verrà visualizzato, altrimenti non compare nella lista di Android Studio)
* L’applicazione è stata testata su Android 6.0 

### Membri del gruppo ###
* ALESSANDRO D’ARMIENTO S231601
* MELISSA COAREZZA S231688

### What is this repository for? ###

* This project aims at the creation of a software able to apply the WSOLA algorithm to perform high quality audio analysis and editing
* Version 0.1a
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests 
* Code review: Professor Antonio Servetti.
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: Alessandro D'Armiento and Melissa Coarezza
* Other community or team contact