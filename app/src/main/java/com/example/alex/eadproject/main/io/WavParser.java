package com.example.alex.eadproject.main.io;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;

import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


import com.example.alex.eadproject.main.core.EADCore;
import com.example.alex.eadproject.main.core.GlobalVariables;
import com.example.alex.eadproject.main.gui.MainActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL;


/**
 * Created by alex on 12/12/16.
 * Reads, writes and manages PCM Wave files
 * Wave files analyzed here http://soundfile.sapp.org/doc/WaveFormat/
 * Little Endian vs Big Endian: https://it.wikipedia.org/wiki/Ordine_dei_byte
 * Some tutorials: https://thiscouldbebetter.wordpress.com/2011/08/14/reading-and-writing-a-wav-file-in-java/
 */

public class WavParser {
    public final static String TAG = "app.io.wavparser";
    private AudioRecord recorder = null;
    private int bufferSize;
    private Thread recordingThread = null, playingThread = null;
    private Context context;
    private boolean isRecording;
    private boolean isPlaying;

    private boolean isEditing;
    private boolean editingDone;
    private String filePath; //path of the file to edit.
    private String playFilePath; //path of the file to reproduce
    private MediaPlayer mediaPlayer;

    public WavParser(final Context context){
        this.context = context;
        bufferSize = AudioRecord.getMinBufferSize
                (GlobalVariables.RECORDER_SAMPLERATE,GlobalVariables.RECORDER_CHANNELS,GlobalVariables.RECORDER_AUDIO_ENCODING)*3;
        isPlaying = false;
        isRecording = false;
        isEditing = false;

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                Log.e(TAG, "Error!");
                return false;
            }

        });
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.start();
                isPlaying = mediaPlayer.isPlaying();
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.reset();
                Intent intent = new Intent(GlobalVariables.AUDIO_SOURCE_COMPLETE);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        });
    }


    /**
     * Create and save wav audio file to disk. Use separate thread
     */
    public void startRecording() {
        if(!isPlaying && !isRecording && !isEditing){
            recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                    GlobalVariables.RECORDER_SAMPLERATE,
                    GlobalVariables.RECORDER_CHANNELS,
                    GlobalVariables.RECORDER_AUDIO_ENCODING,
                    bufferSize);
            int i = recorder.getState();
            if (i==1)
                recorder.startRecording();
            isRecording = true;
            recordingThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    //Log.d(TAG, "recordingThread run: " + System.currentTimeMillis());
                    writeAudioDataToFile();
                }
            }, "AudioRecorder Thread");

            recordingThread.start();
        }
    }


    /**
     * Write sample recorded in a stream to a tmp file
     */
    private void writeAudioDataToFile() {
        byte data[] = new byte[bufferSize];
        String filename = GlobalVariables.getTempFilename();
        FileOutputStream os = null;

        try {
            os = new FileOutputStream(filename);
        } catch (FileNotFoundException e) {
            Log.wtf(TAG, e);
        }

        int read;
        if (null != os) {
            while(isRecording) {
                //Log.d(TAG, "recording: " + System.currentTimeMillis());
                read = recorder.read(data, 0, bufferSize);
                //Log.d(TAG, "read: " + read);
                if (AudioRecord.ERROR_INVALID_OPERATION != read) {
                    try {
                        os.write(data);
                    } catch (IOException e) {
                       Log.wtf(TAG, e);
                    }
                }
            }
            try {
                os.close();
            } catch (IOException e) {
               Log.wtf(TAG, e);
            }
        }
    }


    public void stopRecording() {
        filePath = GlobalVariables.getFilename(); //Remind the file to be edited
        if(isRecording && null != recorder){
            isRecording = false;
            int i = recorder.getState();
            if (i==1)
                recorder.stop();
                recorder.release();
            recorder = null;
            recordingThread = null;
        }
        copyWaveFile(GlobalVariables.getTempFilename(),filePath);
        Log.d(TAG, "File location: " + filePath);

        deleteTempFile();
        playFilePath = filePath;
        //isEditing = true; //Enter edit mode
    }

    private void deleteTempFile() {
        File file = new File(GlobalVariables.getTempFilename());
        file.delete();
    }

    private void copyWaveFile(String inFilename,String outFilename){
        FileInputStream in;
        FileOutputStream out;
        long totalAudioLen = 0;
        long totalDataLen = 36;
        long longSampleRate = GlobalVariables.RECORDER_SAMPLERATE;
        int channels = GlobalVariables.CHANNELS();
        long byteRate = GlobalVariables.RECORDER_BPP * GlobalVariables.RECORDER_SAMPLERATE * channels/8;

        byte[] data = new byte[bufferSize];

        try {
            in = new FileInputStream(inFilename);
            out = new FileOutputStream(outFilename);
            totalAudioLen = in.getChannel().size();
            totalDataLen = totalAudioLen + 36;

            Log.d(TAG ,"File size: " + totalDataLen);

            writeWaveFileHeader(out, totalAudioLen, totalDataLen,
                    longSampleRate, channels, byteRate);

            while(in.read(data) != -1) {
                out.write(data);
            }

            in.close();
            out.close();
            isEditing=false;
        } catch (Exception e) {
            Log.wtf(TAG, e);
        }
    }

    public void reset(){
        isEditing=false;
        isRecording=false;
        isPlaying=false;
        filePath = null;
    }


    public void setFileName(String path){
        filePath = path;
        playFilePath = path;
    }


    /**
     * Adopt "Weighted Overlap and Add" to scale the duration of the data sample just recorded
     * @param multiplier
     * @return
     */
    public void reworkAudioFile(float multiplier){

        Log.d(TAG, "reworkAudioFile: " + filePath);
        Log.d(TAG, "multiplier: " + multiplier);
        String outFilePath="";
        byte[] header;
        Log.d(TAG, "bufferSize: " + bufferSize);
        FileInputStream in;
        FileOutputStream out;
        byte[] data;

        if(null == filePath){
            Log.e(TAG, "Invalid input file!");
        } else {
            //TODO
            //1) Open file OK
            //2) Extract header data and create new header OK
            //3) Get single frame --> Frame goes from pin + 1 to pin + L, with pin shifted each time by L/2 positions OK
            //4) Rework frame OK
            //5) Attach frame to output stream OK
            //6) Save the final stream in a new file OK

            try{
                in = new FileInputStream(filePath);
                outFilePath = filePath.concat("_edit" + multiplier + ".wav");
                out = new FileOutputStream(outFilePath);
                header = extractWaveFileHeader(in);

                //Create new header
                int totalAudioLen = ((header[40] & 0xFF) | ((header[41] & 0xFF) << 8 ) | ((header[42] & 0xFF) << 16 ) | ((header[43] & 0xFF) << 24 ));
                int sampleRate =  ((header[24] & 0xFF) | ((header[25] & 0xFF) << 8 ) | ((header[26] & 0xFF) << 16 ) | ((header[27] & 0xFF) << 24 ));
                int bps = header[34];
                Log.d(TAG, "bps: " + bps);
                if(GlobalVariables.RECORDER_SAMPLERATE != sampleRate){
                    GlobalVariables.RECORDER_SAMPLERATE = sampleRate;
                    GlobalVariables.L = (sampleRate * GlobalVariables.WINDOW_LENGHT_MS*2)/1000;
                    GlobalVariables.S_OUT = GlobalVariables.L/2;
                    GlobalVariables.DELTA_K = (sampleRate/200);
                    Log.d(TAG, "New SampleRate: " + sampleRate);
                    Log.d(TAG, "New L: " + GlobalVariables.L);
                    Log.d(TAG, "New DELTA_K: " + GlobalVariables.DELTA_K);
                    Log.d(TAG, "New S_OUT: " + GlobalVariables.S_OUT);
                }
                EADCore.getInstance().adjustShift(multiplier);
                // totalAudioLen == NumSamples * NumChannels * BitsPerSample/8
                EADCore.getInstance().init(totalAudioLen/(GlobalVariables.CHANNELS()*2)); //Initialize input sample array
                totalAudioLen = Math.round((float)totalAudioLen*multiplier);
                int totalDataLen = totalAudioLen + 36;
                long longSampleRate = GlobalVariables.RECORDER_SAMPLERATE;
                int channels = GlobalVariables.CHANNELS();
                long byteRate = GlobalVariables.RECORDER_BPP * GlobalVariables.RECORDER_SAMPLERATE * channels/8;
                writeWaveFileHeader(out, totalAudioLen, totalDataLen, longSampleRate,
                        channels, byteRate);
                //Header created

                long startTime = System.currentTimeMillis();
                Log.d(TAG, "Reading start: " + startTime);
                data = new byte[2]; //TODO: this is a performance killer. Find a better way
                while(-1 != in.read(data)){
                    EADCore.getInstance().pushSample(Sample.makeSample(data));
                }
                Log.d(TAG, "Reading done in: " + (System.currentTimeMillis() - startTime)/1000 + " s");
                in.close(); //No more need this.

                startTime = System.currentTimeMillis();
                Log.d(TAG, "Computation start: " + startTime);
                Log.d(TAG, "totalAudioLen: " + totalAudioLen);
                Log.d(TAG,"totalAudioLen/(GlobalVariables.CHANNELS()*2): " + totalAudioLen/(GlobalVariables.CHANNELS()*2));
                ArrayList<Short> outputSignal = EADCore.getInstance().makeOutputSignal(totalAudioLen/(GlobalVariables.CHANNELS()*2));
                Log.d(TAG, "Computation done in: " + (System.currentTimeMillis() - startTime)/1000+ " s");

                startTime = System.currentTimeMillis();
                Log.d(TAG, "Writing start: " + startTime);
                for(Short s : outputSignal){
                    out.write(Sample.makeBytes(s)); //TODO: This is another performance killer
                }
                Log.d(TAG, "Writing done in: " + (System.currentTimeMillis() - startTime)/1000 + " s");
                out.close();

            } catch (Exception e){
                Log.wtf(TAG, e);
                e.printStackTrace();
            } finally {
                Log.d(TAG, "process ended");
                isEditing = false;
                editingDone = true;
                playFilePath = outFilePath;
            }

        }
    }


    /**
     * Create standard WAV header
     * @param out
     * @param totalAudioLen
     * @param totalDataLen
     * @param longSampleRate
     * @param channels
     * @param byteRate
     * @throws IOException
     */
    private void writeWaveFileHeader(
            FileOutputStream out, long totalAudioLen,
            long totalDataLen, long longSampleRate, int channels,
            long byteRate) throws IOException
    {
        byte[] header = new byte[44];

        header[0] = 'R';  // RIFF/WAVE header
        header[1] = 'I';
        header[2] = 'F';
        header[3] = 'F';
        header[4] = (byte) (totalDataLen & 0xff);
        header[5] = (byte) ((totalDataLen >> 8) & 0xff);
        header[6] = (byte) ((totalDataLen >> 16) & 0xff);
        header[7] = (byte) ((totalDataLen >> 24) & 0xff);
        header[8] = 'W';
        header[9] = 'A';
        header[10] = 'V';
        header[11] = 'E';
        header[12] = 'f';  // 'fmt ' chunk
        header[13] = 'm';
        header[14] = 't';
        header[15] = ' ';
        header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
        header[17] = 0;
        header[18] = 0;
        header[19] = 0;
        header[20] = 1;  // format = 1
        header[21] = 0;
        header[22] = (byte) channels;
        header[23] = 0;
        header[24] = (byte) (longSampleRate & 0xff);
        header[25] = (byte) ((longSampleRate >> 8) & 0xff);
        header[26] = (byte) ((longSampleRate >> 16) & 0xff);
        header[27] = (byte) ((longSampleRate >> 24) & 0xff);
        header[28] = (byte) (byteRate & 0xff);
        header[29] = (byte) ((byteRate >> 8) & 0xff);
        header[30] = (byte) ((byteRate >> 16) & 0xff);
        header[31] = (byte) ((byteRate >> 24) & 0xff);
        header[32] = (byte) (2 * 16 / 8);  // block align
        header[33] = 0;
        header[34] = GlobalVariables.RECORDER_BPP;  // bits per sample
        header[35] = 0;
        header[36] = 'd';
        header[37] = 'a';
        header[38] = 't';
        header[39] = 'a';
        header[40] = (byte) (totalAudioLen & 0xff);
        header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
        header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
        header[43] = (byte) ((totalAudioLen >> 24) & 0xff);
        out.write(header, 0, 44);
    }

    /**
     * Extract header bytes into byte Array.
     * Fixed size as Wave standard format.
     */
    private byte[] extractWaveFileHeader(FileInputStream in){
        byte[] header = new byte[44];
        try{
            in.read(header, 0, 44);
        } catch (Exception e){
            Log.wtf(TAG, e);
        }
        //printDebugStuff("Header", header);
        return header;
    }

    private void printDebugStuff(String kind, byte[] data){
        Log.d(TAG, "printDebugStuff: " + kind);
        Log.d(TAG, "data has size: " + data.length);
        for(int i = 0; i<data.length; i++){
            Log.d(TAG, "data["+i+"]: " + data[i]);
        }
    }

    public void startPlaying() {
        if(!isRecording && !isPlaying){

            playingThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "recordingThread run: " + System.currentTimeMillis());
                    play();
                }
            }, "AudioPlayer Thread");

            playingThread.start();
        }
    }

    private void play(){
        mediaPlayer.reset();
        try{
            mediaPlayer.setDataSource(playFilePath);
        } catch (Exception e){
            e.printStackTrace();
            Log.wtf(TAG, e);
        }
        mediaPlayer.prepareAsync();

    }

    public void stopPlaying() {
        if(isPlaying){
            if(mediaPlayer != null && mediaPlayer.isPlaying()){
                mediaPlayer.stop();
                //isPlaying = false;
                isPlaying = false;
                //playingThread.stop();
                playingThread = null;
            }
        }
    }

    public boolean isPlaying(){
        return isPlaying;
    }

}
