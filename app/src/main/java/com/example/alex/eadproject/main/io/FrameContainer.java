package com.example.alex.eadproject.main.io;

import android.util.Log;

import com.example.alex.eadproject.main.core.GlobalVariables;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 27/12/2016.
 */

public class FrameContainer {
    public final static String TAG = "FrameContainer";
    private ArrayList<Double> frame;
    private ArrayList<Double> referenceFrame;
    private ArrayList<Double> analysisWindow;

    private int frameCounter=0; //0 for first, -1 for last
    private int coreFrameSize=0;

    private int startPosition=0; //index of this frame in the inputSignal
    private int startPositionClean=0; //index of the analysisWindow in the inputSignal

    public FrameContainer(List<Short> inputFrame, int frameCounter, int pIn){
        frame = new ArrayList<>(inputFrame.size());
        for(int i = 0; i<inputFrame.size(); i++){
            frame.add(i, (double)inputFrame.get(i));
            //Log.d(TAG, "sample value: " + inputFrame.get(i));
            //DEBUG force a constant
            //frame.add(i, 5000.0);
        }
        this.frameCounter = frameCounter;

        switch (frameCounter){
            case 0: //First frame
                coreFrameSize = frame.size();
                startPosition = 0;
                break;
            case -1: //Last frame
                coreFrameSize = frame.size() - GlobalVariables.DELTA_K;
                startPosition = pIn - GlobalVariables.DELTA_K;
                break;
            default: //All the others
                coreFrameSize = frame.size() - 2*GlobalVariables.DELTA_K;
                startPosition = pIn - GlobalVariables.DELTA_K;
                break;
        }
        //Log.d(TAG, "Frame created. Size: " + frame.size() + ", coreFrameSize: " + coreFrameSize);
        //Log.d(TAG, "startPos: " + startPosition + ", frameCounter: " + frameCounter);
    }

    public ArrayList<Double> getFrame() {
        return frame;
    }

    public int getFrameCounter() {
        return frameCounter;
    }

    public int getCoreFrameSize() {
        //Log.d(TAG, "getCoreFrameSize: " + coreFrameSize);
        return coreFrameSize;
    }

    public boolean isFirstFrame(){
        return frameCounter == 0;
    }

    public boolean isLastFrame(){
        return frameCounter == -1;
    }

    public ArrayList<Double> getReferenceFrame() {
        //Log.d(TAG, "getReferenceFrame. Size: " + referenceFrame.size());
        return referenceFrame;
    }

    public void setReferenceWindow(List<Short> reference) {
        //Log.d(TAG, "setReferenceWindow. Input size: " + reference.size());
        referenceFrame = new ArrayList<>(reference.size());
        for(int i = 0; i< reference.size(); i++){
            referenceFrame.add(i, (double)reference.get(i));
        }
        //Log.d(TAG, "Created ReferenceWindow. Size: " + reference.size());
    }

    public ArrayList<Double> getAnalysisWindow() {
        //Log.d(TAG, "getAnalysisWindow. Size: " + analysisWindow.size());
        return analysisWindow;
    }

    public void setAnalysisWindow(int j) {
        //Log.d(TAG, "analysis windows goes from: " + j + " to " + ( coreFrameSize + j));
        this.analysisWindow = new ArrayList<>(frame.subList(Math.max(0,j), Math.min(frame.size()-1 ,coreFrameSize + j)));
        startPositionClean = startPosition + j;
        //Log.d(TAG, "Created analysisWindow. Size: " + analysisWindow.size() + ", j: " + j);
    }

    public int getStartPosition() {
        return startPosition;
    }

    public int getStartPositionClean() {
        //Log.d(TAG, "getStartPositionClean: " + startPositionClean);
        return startPositionClean;
    }
}
