package com.example.alex.eadproject.main.core;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.example.alex.eadproject.main.gui.MainActivity;
import com.example.alex.eadproject.main.io.Correlation;
import com.example.alex.eadproject.main.io.FileUtils;
import com.example.alex.eadproject.main.io.FrameContainer;
import com.example.alex.eadproject.main.io.WavParser;
import java.util.ArrayList;


/**
 * Created by alex on 12/12/16.
 * Singleton managing all the core features of the projects
 */

public class EADCore {

    public final static String TAG = "app.core.EADCore";
    private static EADCore mInstance = null;
    private WavParser wavParser;
    private ArrayList<Short> inputSignal;
    private Activity mainActivity; //Meh


    private EADCore(Context context){
        wavParser = new WavParser(context);
        inputSignal = new ArrayList<>();
    }


    public void setMainActivity(Activity activity){
        this.mainActivity = activity;
    }

    public static synchronized EADCore getInstance(Context context){
        if(null==mInstance){
            mInstance = new EADCore(context);
        }
        return mInstance;
    }


    public static synchronized EADCore getInstance(){
        return mInstance;
    }

    public WavParser getWavParser() {
        return wavParser;
    }


    /**
     * Check if all the permissions have been granted. Return false if at least a permission is denied
     * @param context
     * @return
     */
    public boolean checkPermissions(Context context){
        if(PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            return false;
        if(PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO))
            return false;
        if(PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(context, Manifest.permission.VIBRATE))
            return false;
        return true;
    }

    public void requestPermissions(Activity activity){
       ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.VIBRATE}, 3);
    }

    public void reset(){
        inputSignal = new ArrayList<>();
    }


    public void init(int size){
        Log.d(TAG, "init: " + size);
        //The list is already created to the size of the signal to avoid perfomance killing resize
        inputSignal = new ArrayList<>(size);
    }

    public void pushSample(short s){
        inputSignal.add(s);
    }


    /**
     * WorkFlow:
     * 1)While the input signal has samples:
     *      Extract a frame of L (800 samples) + 2*DeltaK (200 samples) into a FrameContainer obj
     *      Select from the given frame a frame of 20ms (800 samples) by mean of the highest correlation with the previous frame
     *      Apply twice the Von Hann Windows (multiply for a sine wave twice.) This pulls down all values far from the frame center
     *      Sum the frame to the output signal by mean of OLA algorithm.
     *      Increment S_IN by a computed value based on how much the input signal has to be stretched
     *      Increment S_OUT by a fixed value of L/2 (half the frame size)
     * 2)When the input signal has been fully processed:
     *      Quantize the array of fp into an array of 16 bit signed int
     *      Give the quantized array to the parser to make a Wave file from it
     */
    public ArrayList<Short> makeOutputSignal(int expectedSize){
        Log.d(TAG, "makingOutputSignal: " + expectedSize);
        FrameContainer frameContainer=null; //This creates and contains the analysisWindow and metadata
        ArrayList<Double> outputSignal = new ArrayList<>(expectedSize);//The final signal is initialized to the size of the final duration to allow faster operations
        ArrayList<Double> referenceFrame = null; //This is used to find correlation
        ArrayList<Double> synthesisFrame=null;
        int pIn=0, pOut=0, frameCounter=0;


        Log.d(TAG, "inputSignal.size: " + inputSignal.size());
        while(pIn<inputSignal.size()){
            try{
                frameContainer = extractFrame(pIn, frameCounter); //Create a proto-analysisWindow
                frameCounter++; //Update frameCounter. Used for gui display
                if(frameCounter%50 == 0){
                    ((MainActivity)mainActivity).updateComputation(pIn, inputSignal.size());
                }
            } catch (Exception e){
                Log.wtf(TAG, e);
                e.printStackTrace();
            }
            try{
                //computeRelatedFrame(frameContainer, referenceFrame); //Create the analysisWindow from the protoAW and the referenceFrame
                computeRelatedFrameExp(frameContainer, referenceFrame);
                if(!frameContainer.isLastFrame()) //Create referenceFrame for the next iteration
                    referenceFrame = frameContainer.getReferenceFrame();
            } catch (Exception e){
                Log.wtf(TAG, e);
                e.printStackTrace();
            }
            try{
                synthesisFrame = hanningWindowFp(frameContainer.getAnalysisWindow(), 1);//Apply the Hanning Window to the input frame. Twice.
            } catch (Exception e){
                Log.wtf(TAG, e);
                e.printStackTrace();
            }
            try{
                Log.d(TAG, "Copying into output");
                for(int k = 0; k<synthesisFrame.size(); k++){ //Copy inputFrame into outputSignal
                    if(outputSignal.size()>(k+pOut)){
                        //Log.d(TAG, "Old " + (pOut + k) + "value: " + outputSignal.get(pOut+k));
                        outputSignal.set(pOut + k, outputSignal.get(pOut + k) + synthesisFrame.get(k)); //Update sample
                        //Log.d(TAG, "New " + (pOut + k) + " value: " + outputSignal.get(pOut+k));
                        //Log.d(TAG, "" + outputSignal.get(pOut+k));
                    } else {
                        outputSignal.add(synthesisFrame.get(k)); //Create new sample
                        //Log.d(TAG, "Added at position " + pOut + " value: " + outputSignal.get(outputSignal.size()-1));
                    }
                }
            } catch (Exception e){
                Log.wtf(TAG, e);
                e.printStackTrace();
            }
            pOut += GlobalVariables.S_OUT; //Shifts the output index
            pIn += GlobalVariables.S_IN; //Shifts the input index
        }
        ((MainActivity)mainActivity).endComputation();
        return quantize(outputSignal); //Return a 16bit signed integer quantized array from outputSignal
    }

    /**
     * Create a new frameContaner with a portion of the inputSignal and useful metadata
     * @param pIn
     * @param frameCounter
     * @return
     */
    private FrameContainer extractFrame(int pIn, int frameCounter){
        FrameContainer frameContainer;
        int start, end;
        if(0==pIn){
            Log.d(TAG, "first frame!");   //First frame
            frameContainer = new FrameContainer(inputSignal.subList(0, GlobalVariables.L), 0, 0);
        } else if(pIn+GlobalVariables.L > inputSignal.size()){
            Log.d(TAG, "Last frame! pIn: " + pIn + ", inputSignal.size(): " + inputSignal.size()); //Last frame
            start = pIn - GlobalVariables.DELTA_K;
            end = inputSignal.size()-1;
            frameContainer = new FrameContainer(inputSignal.subList(start, end), -1, pIn);
        }
        else {
            //All the other frames
            start = Math.max(0, (pIn - GlobalVariables.DELTA_K)); //IMPORTANT: L must be greater than DK
            end = Math.min(inputSignal.size(),(pIn + GlobalVariables.L + GlobalVariables.DELTA_K));
            //Log.d(TAG, "extractFrame read pIn: " + pIn);
            //Log.d(TAG, "start: "  + start + ", end: " + end);
            frameContainer = new FrameContainer(inputSignal.subList(start, end), frameCounter, pIn);
        }
        return frameContainer;
    }


    private FrameContainer computeRelatedFrameExp(FrameContainer fc, ArrayList<Double> referenceFrame){

        if(fc.isFirstFrame() || null == referenceFrame){
            Log.d(TAG, "First frame. Skipping correlation research");
            fc.setReferenceWindow(inputSignal.subList(fc.getCoreFrameSize() - GlobalVariables.S_OUT,
                    (fc.getCoreFrameSize()*2)- GlobalVariables.S_OUT));
            fc.setAnalysisWindow(0); //Analysis frame is the frame itself
        } else {
            int bestOffset = Correlation.seekBestOverlapPositionExp(fc.getFrame(), referenceFrame);

            try {  //Now purge away the unwanted samples and create referenceFrame for the nextCheck
                fc.setAnalysisWindow(bestOffset);
                if (!fc.isLastFrame()) { //Set reference frame for next iteration, only if this is not the last frame
                    int start = fc.getStartPositionClean() + fc.getCoreFrameSize() - GlobalVariables.S_OUT; //Start position of the very next frame of the analysisWindow
                    int end = Math.min(inputSignal.size(), start + fc.getCoreFrameSize()); //Prevent IOOBE
                    fc.setReferenceWindow(inputSignal.subList(start, end));
                    //Log.d(TAG, "referenceFrame set. start: " + start + ", end: " + end);
                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "total input size is: " + inputSignal.size());
            }
        }
        return fc;
    }
 /**
     * Apply the Von Hann Window function to the given signal a defined number of times
     * @param inputFrame is the input signal.
     * @param cycles the number of thimes the computation is performed on the same signal.
     *               It is a multiplicative factor.
     *               Got from stackoverlow http://stackoverflow.com/questions/11600515/hanning-von-hann-window
     * @return
     */
    public ArrayList<Double> hanningWindowFp(ArrayList<Double> inputFrame, int cycles)
    {

        for(int j = 0; j<cycles; j++){
            for (int i = 0; i < inputFrame.size(); i++)
            {
                inputFrame.set(i, inputFrame.get(i) * 0.5 * (1.0 - Math.cos((2.0 * Math.PI * i)/(inputFrame.size()-1))));
            }
        }

        return inputFrame;
    }


    /**
     * Adjust S_IN value to correct frame shift
     * Fixed S_OUT to L/2, to double the signal duration S_IN should be L/5, to halve it, S_IN should
     *      be 3/2 L.
     */
    public void adjustShift(float multiplier){
        Log.d(TAG, "adjustShift: " + multiplier);
        //GlobalVariables.S_IN = Math.round(GlobalVariables.S_OUT/multiplier); //Non funziona: Se 2x il segnale in uscita è 5/3 del segnale in ingresso. Se 0.5x il segnale in uscita è 3/5 del segnale in ingresso
        //GlobalVariables.S_IN = Math.round(GlobalVariables.S_OUT); //1x
        //GlobalVariables.S_IN = Math.round((GlobalVariables.S_OUT) *2 * 0.2); //2x
        //GlobalVariables.S_IN = Math.round((GlobalVariables.S_OUT) *3 * 0.5); //0.5x
        GlobalVariables.S_OUT = GlobalVariables.L/2;
        if(1==multiplier){
            GlobalVariables.S_IN = GlobalVariables.S_OUT;
        } else {
            if(multiplier>1){
                //Y = (-3/10) * X + (4/5) ---> x is multiplier.
                GlobalVariables.S_IN = Math.round(((-0.3)*multiplier + 0.8)*GlobalVariables.L);
            } else { //<1
                //Y = -1 * X + (3/2) //Circa. In reltà è un numero compreso tra L 6/5L
                GlobalVariables.S_IN = Math.round((-multiplier + 1.5)*GlobalVariables.L);
            }
        }
        Log.d(TAG, "Shift Adjusted. L: " + GlobalVariables.L + ", S_IN: " + GlobalVariables.S_IN + ", S_OUT: " + GlobalVariables.S_OUT);
    }

    public ArrayList<Short> quantize(ArrayList<Double> inputSignal){
        ArrayList<Short> outputSignal = new ArrayList<>(inputSignal.size());
        try{
            for(int i = 0; i<inputSignal.size(); i++){
                //Log.d(TAG, "input: " + inputSignal.get(i));
                //Log.d(TAG, "output: " + (short) Math.round(inputSignal.get(i)));
                //Log.d(TAG, "error: " + (inputSignal.get(i) - (short)Math.round(inputSignal.get(i))));
                outputSignal.add(i, (short)Math.round(inputSignal.get(i)));
            }
        } catch (Exception e){
            Log.wtf(TAG, e);
            e.printStackTrace();
        }
        return outputSignal;
    }



    public void setFilePath(Context context, Uri uri){
        Log.d(TAG, "setFilePath: " + uri.toString());
        try{
            String path = FileUtils.getPath(context, uri);
            Log.d(TAG, "File Path: " + path);
            wavParser.setFileName(path);
        } catch (Exception e){
            e.printStackTrace();
            Log.wtf(TAG, e);
        }
    }


}
