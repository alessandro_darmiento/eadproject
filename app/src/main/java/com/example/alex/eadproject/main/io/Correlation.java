package com.example.alex.eadproject.main.io;

import android.util.Log;

import com.example.alex.eadproject.main.core.GlobalVariables;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Alex on 27/12/2016.
 * Static Class to manage correlation methods
 */

public class Correlation {
    public static final String TAG = "src.io.correlation";

    /**
     * Computes the cross correlation between sequences a and b.
     */
    public static double[] xcorr(double[] a, double[] b)
    {
        int len = a.length;
        if(b.length > a.length)
            len = b.length;

        return xcorr(a, b, len-1);
    }

    /**
     * Computes the cross correlation between sequences a and b.
     */
    public static double[] xcorr(ArrayList<Double> a, ArrayList<Double> b)
    {

        //double[] arr = a.stream().mapToDouble(i -> i).toArray(); Android delle palle usa java 7 quindi chiami una lambda expression ma ti risponde picche
        //double[] brr = b.stream().mapToDouble(i->i).toArray();

        double[] arr = new double[a.size()];
        for(int i=0; i<a.size();i++)
            arr[i] = a.get(i);

        double[] brr = new double[b.size()];
        for(int i=0; i<b.size();i++)
            brr[i] = b.get(i);

        int len = arr.length;
        if(brr.length > arr.length)
            len = brr.length;

        return xcorr(arr, brr, len-1);
    }

    public static double singleXcorr(ArrayList<Double> a, ArrayList<Double> b){
        double[] corr = xcorr(a, b);
        double avg = 0;
        for(double d : corr)
            avg+=d;
        avg = avg/corr.length;
        return avg;
    }


    /**
     * Computes the cross correlation between sequences a and b.
     * maxlag is the maximum lag to
     */
    public static double[] xcorr(double[] a, double[] b, int maxlag)
    {
        double[] y = new double[2*maxlag+1];
        Arrays.fill(y, 0);

        for(int lag = b.length-1, idx = maxlag-b.length+1;
            lag > -a.length; lag--, idx++)
        {
            if(idx < 0)
                continue;

            if(idx >= y.length)
                break;

            // where do the two signals overlap?
            int start = 0;
            // we can't start past the left end of b
            if(lag < 0)
            {
                //System.out.println("b");
                start = -lag;
            }

            int end = a.length-1;
            // we can't go past the right end of b
            if(end > b.length-lag-1)
            {
                end = b.length-lag-1;
                //System.out.println("a "+end);
            }

            //System.out.println("lag = " + lag +": "+ start+" to " + end+"   idx = "+idx);
            for(int n = start; n <= end; n++)
            {
                //System.out.println("  bi = " + (lag+n) + ", ai = " + n);
                y[idx] += a[n]*b[lag+n];
            }
            //System.out.println(y[idx]);
        }

        return(y);
    }

    //Cross correlation finder from StackOverflow: http://stackoverflow.com/questions/28428365/how-to-find-correlation-between-two-integer-arrays-in-java
    public static double bozoCorrelation(ArrayList<Double> xs, ArrayList<Double> ys) {

        if((null == xs || null==ys )||(xs.isEmpty() || ys.isEmpty()) || (xs.size()!=ys.size()))
            return 0;

        double sx = 0.0;
        double sy = 0.0;
        double sxx = 0.0;
        double syy = 0.0;
        double sxy = 0.0;

        int n = xs.size();

        for(int i = 0; i < n; i++) {
            double x = xs.get(i);
            double y = ys.get(i);

            sx += x;
            sy += y;
            sxx += x * x;
            syy += y * y;
            sxy += x * y;
        }

        // covariation
        double cov = sxy / n - sx * sy / n / n;
        // standard error of x
        double sigmax = Math.sqrt(sxx / n -  sx * sx / n / n);
        // standard error of y
        double sigmay = Math.sqrt(syy / n -  sy * sy / n / n);

        // correlation is just a normalized covariation
        //Log.d(TAG, "corr: " + (cov / sigmax / sigmay));
        return cov / sigmax / sigmay;
    }


    public static double provaCorrelation(ArrayList<Double> xs, ArrayList<Double> ys){
        double result = 0;
        double sumx=0, sumy=0, sumxy=0, sumx2=0, sumy2=0;
        double ex2 = 0, ey2 = 0;
        if((null == xs || null==ys )||(xs.isEmpty() || ys.isEmpty()) || (xs.size()!=ys.size()))
            return 0;

        ArrayList<Double> xy = new ArrayList<>(xs);
        ArrayList<Double> y2 = new ArrayList<>(ys);
        ArrayList<Double> x2 = new ArrayList<>(xs);

        for(int i =0; i<xs.size(); i++){
            xy.set(i, xy.get(i)*ys.get(i));
            x2.set(i,x2.get(i) *x2.get(i));
            y2.set(i,y2.get(i) *y2.get(i));
            sumx += xs.get(i);
            sumy += ys.get(i);
            sumxy += xy.get(i);
            sumx2 += x2.get(i);
            sumy2 += y2.get(i);
        }

        ex2 = sumx * sumx;
        ey2 = sumy * sumy;

        result = (xs.size()*sumxy - sumx*sumy)/(Math.sqrt((xs.size()*sumx2 - ex2)*(xs.size() * sumy2 - ey2)));
        //Log.d("Correlation", "provaresult: " + result);

        return result;
    }

    public static int seekBestOverlapPositionExp(ArrayList<Double> inputFrame, ArrayList<Double> referenceFrame){
        Log.d(TAG, "inputFrame.size: " + inputFrame.size());
        Log.d(TAG, "referenceFrame.size: " + referenceFrame.size());
        int bestOffset = 0;
        int i=0;
        Double[] a, b;
        double currentCorr=0, maxCorr=0;

        b = new Double[referenceFrame.size()];
        a = new Double[referenceFrame.size()];
        referenceFrame.toArray(b);
        for(i = 0; i < inputFrame.size() - referenceFrame.size() +1; i++){
            List<Double> l = inputFrame.subList(i, referenceFrame.size() + i);
            l.toArray(a);
            //currentCorr = normXCorr(a, b);
            currentCorr = normXCorr(a,b, b.length/5);
            if(currentCorr>maxCorr){
                bestOffset = i;
            }
        }
        Log.d(TAG, "BestOffset: " + bestOffset);
        return bestOffset;
    }

    /**
     * Normalized Correlation between two array of the same length
     * @param a
     * @param b
     * @return
     */
    private static double normXCorr(Double[] a, Double[] b){
        //Log.d(TAG, "normXCorr. a.lenght: " + a.length + ", b.length: " + b.length);
        double corr = 0;
        double norm = 0;
        for (int i = 0; i < b.length; i ++){
                corr += b[i] * a[i];
                norm += b[i] * b[i];
        }
        // To avoid division by zero.
        if (norm < 1e-8){
            norm = 1.0;
        }
        return corr / Math.pow(norm,0.5);
    }

    /**
     * Normalized correlation between the initial part of two arrays
     * @param a
     * @param b
     * @param maxOverlapRegion
     * @return
     */
    private static double normXCorr(Double[] a, Double[] b, int maxOverlapRegion){
        //Log.d(TAG, "normXCorr. a.lenght: " + a.length + ", b.length: " + b.length);
        double corr = 0;
        double norm = 0;
        if(maxOverlapRegion>b.length){
            maxOverlapRegion = b.length;
        }
        for (int i = 0; i < maxOverlapRegion; i ++){
            corr += b[i] * a[i];
            norm += b[i] * b[i];
        }
        // To avoid division by zero.
        if (norm < 1e-8){
            norm = 1.0;
        }
        return corr / Math.pow(norm,0.5);
    }



    public static int seekBestOverlapPosition(ArrayList<Double> inputFrame, ArrayList<Double> referenceFrame){
       // Log.d(TAG, "Seek Best Overlap Position");
        int bestOffset;
        double bestCorrelation, currentCorrelation;
        int tempOffset;
        int comparePosition = 0;

        bestCorrelation = -10;
        bestOffset = 0;
        int seekLength = GlobalVariables.DELTA_K;



        for(tempOffset = 0; tempOffset< seekLength; tempOffset++){
            comparePosition = tempOffset;

            // Calculates correlation value for the mixing position
            // corresponding
            // to 'tempOffset'
            currentCorrelation = calcCrossCorr(inputFrame, referenceFrame, comparePosition);
            // heuristic rule to slightly favor values close to mid of the
            // range
            double tmp = (double) (2 * tempOffset - seekLength) / seekLength;
            currentCorrelation = ((currentCorrelation + 0.1) * (1.0 - 0.25 * tmp * tmp));
            //Log.d(TAG, "currentCorr: " + currentCorrelation);

            // Checks for the highest correlation value
            if (currentCorrelation > bestCorrelation) {
                bestCorrelation = currentCorrelation;
                bestOffset = tempOffset;
            }

        }
       // Log.d(TAG, "Best offset: " + bestOffset);
        return bestOffset;
    }
/*
    /**
     * Seeks for the optimal overlap-mixing position.
     *
     * The best position is determined as the position where the two overlapped
     * sample sequences are 'most alike', in terms of the highest
     * cross-correlation value over the overlapping period
     *
     * @param inputBuffer The input buffer
     * @param postion The position where to start the seek operation, in the input buffer.
     * @return The best position.
     *
    private int seekBestOverlapPosition(float[] inputBuffer, int postion) {
        int bestOffset;
        double bestCorrelation, currentCorrelation;
        int tempOffset;

        int comparePosition;

        // Slopes the amplitude of the 'midBuffer' samples
        precalcCorrReferenceMono();

        bestCorrelation = -10;
        bestOffset = 0;


        // Scans for the best correlation value by testing each possible
        // position
        // over the permitted range.
        for (tempOffset = 0; tempOffset < seekLength; tempOffset++) {

            comparePosition = postion + tempOffset;

            // Calculates correlation value for the mixing position
            // corresponding
            // to 'tempOffset'
            currentCorrelation = (double) calcCrossCorr(pRefMidBuffer, inputBuffer,comparePosition);
            // heuristic rule to slightly favor values close to mid of the
            // range
            double tmp = (double) (2 * tempOffset - seekLength) / seekLength;
            currentCorrelation = ((currentCorrelation + 0.1) * (1.0 - 0.25 * tmp * tmp));

            // Checks for the highest correlation value
            if (currentCorrelation > bestCorrelation) {
                bestCorrelation = currentCorrelation;
                bestOffset = tempOffset;
            }
        }

        return bestOffset;

    }

    /*
     * Slopes the amplitude of the 'midBuffer' samples so that cross correlation
     * is faster to calculate. Why is this faster?
     *
    void precalcCorrReferenceMono()
    {
        for (int i = 0; i < overlapLength; i++){
            float temp = i * (overlapLength - i);
            pRefMidBuffer[i] = pMidBuffer[i] * temp;
        }
    }


    double calcCrossCorr(float[] mixingPos, float[] compare, int offset){
        double corr = 0;
        double norm = 0;
        for (int i = 1; i < GlobalVariables.DELTA_K; i ++){
            corr += mixingPos[i] * compare[i + offset];
            norm += mixingPos[i] * mixingPos[i];
        }
        // To avoid division by zero.
        if (norm < 1e-8){
            norm = 1.0;
        }
        return corr / Math.pow(norm,0.5);
    }
*/
    private static double calcCrossCorr(ArrayList<Double> mixingPos, ArrayList<Double> compare, int offset){
        double corr = 0;
        double norm = 0;
        for (int i = 1; i < GlobalVariables.DELTA_K && i<mixingPos.size() && (i + offset)< compare.size(); i ++){
            corr += mixingPos.get(i) * compare.get(i + offset);
            norm += mixingPos.get(i) * mixingPos.get(i);
        }
        // To avoid division by zero.
        if (norm < 1e-8){
            norm = 1.0;
        }
        return corr / Math.pow(norm,0.5);
    }


}
