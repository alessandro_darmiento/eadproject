package com.example.alex.eadproject.main.io;


/**
 * Created by Alex on 13/12/2016.
 * A 16 bit sample made from 2 byte in little endian notation and viceversa
 */

public class Sample {
    public static short makeSample(byte[] b){
        return (short)((b[0] & 0xFF) | ((b[1] & 0xFF) << 8 ));
    }

    public static byte[] makeBytes(short s){
        byte[] b = new byte[2];
        b[0] = (byte)(s & 0xff);
        b[1] = (byte) ((s >> 8) & 0xff);
        return b;
    }
}
