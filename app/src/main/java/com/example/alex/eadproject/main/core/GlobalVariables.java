package com.example.alex.eadproject.main.core;

import android.media.AudioFormat;
import android.os.Environment;

import java.io.File;

/**
 * Created by alex on 12/12/16.
 * Global Variables and Constants
 */

public class GlobalVariables {
    public final static String major = "0";
    public final static String minor = "7a";

    public final static String AUDIO_SOURCE_COMPLETE = "audio_source_complete";

    public static final int RECORDER_BPP = 16; //16 bits per sample
    public static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
    public static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
    public static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";
    public static int RECORDER_SAMPLERATE = 8000; //8000Hz for wide band voice quality (Not final because can be edited by an input file)
    //public static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_STEREO; //2 channels
    public static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO; //1 channel
    public static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT; //PCM 16 bits per sample per channel

    public static final int WINDOW_LENGHT_MS = 50; //50ms
    public static int DELTA_K = RECORDER_SAMPLERATE/200;

    //public static final int DELTA_K = 0;

    public static int L = (RECORDER_SAMPLERATE * WINDOW_LENGHT_MS)/1000; //TODO: how to find exact window width?
    public static int S_OUT = L/2;
    public static long S_IN = L; //TODO: apply multiplier to this

    public static final double CORRELATION_THRESHOLD = 0.9; //TODO: tweak this

    public static String getFilename(){
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }

        return (file.getAbsolutePath() + "/record" + System.currentTimeMillis() +
                AUDIO_RECORDER_FILE_EXT_WAV);
    }

    public static String getTempFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }

        File tempFile = new File(filepath,AUDIO_RECORDER_TEMP_FILE);

        if (tempFile.exists())
            tempFile.delete();

        return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
    }

    /**
     * @return the number of channels listened
     */
    public static int CHANNELS(){
        if(RECORDER_CHANNELS == AudioFormat.CHANNEL_IN_MONO)
            return 1;
        if(RECORDER_CHANNELS == AudioFormat.CHANNEL_IN_STEREO)
            return 2;
        return 0;
    }
}
