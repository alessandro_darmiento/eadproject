package com.example.alex.eadproject.main.gui;

 import android.app.ProgressDialog;
 import android.content.BroadcastReceiver;
 import android.content.Context;
 import android.content.Intent;
 import android.content.IntentFilter;
 import android.net.Uri;
 import android.os.Vibrator;
 import android.support.v4.content.ContextCompat;
 import android.support.v4.content.LocalBroadcastManager;
 import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
 import android.widget.TextView;
import android.widget.Toast;

 import com.example.alex.eadproject.R;
import com.example.alex.eadproject.main.core.EADCore;
 import com.example.alex.eadproject.main.core.GlobalVariables;
 import com.example.alex.eadproject.main.io.WavParser;

public class MainActivity extends AppCompatActivity {
    public static String TAG = "app.gui.main";
    private final static int FILE_SELECT_CODE = 0;


    private ImageButton recordButton;
    private ImageButton playButton;
    private ImageButton browseButton;
    private Button startButton;
    private SeekBar seekBar;
    private TextView currentProgress;
    private TextView version;

    private MainActivity mInstance;
    private ProgressDialog progressDialog;


    private boolean isEditing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        mInstance = this;
        isEditing = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EADCore.getInstance(this).setMainActivity(this);


        //ProgressDialog stuff
        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMessage(getResources().getString(R.string.loading));


        //Bind the views
        recordButton = (ImageButton) findViewById(R.id.main_record_button);
        playButton =(ImageButton) findViewById(R.id.main_play_button);
        startButton = (Button) findViewById(R.id.main_start_button);
        browseButton = (ImageButton) findViewById(R.id.main_browse_button);
        seekBar = (SeekBar) findViewById(R.id.main_seek_bar);
        currentProgress = (TextView) findViewById(R.id.main_current_progress);
        version = (TextView)findViewById(R.id.main_version);

        seekBar.setProgress(50); //An offset of 50 is always added to make progress range from 50 to 200

        version.setText("V. " + GlobalVariables.major  + "." + GlobalVariables.minor); //TODO: fare meglio sto schifo

        //Set multiplier
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float displayProgress = ((float)(progress+50)/100);
                currentProgress.setText(getString(R.string.curr_multiplier).replace("%f", String.valueOf(displayProgress)));
                EADCore.getInstance().adjustShift(displayProgress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        //Record
        recordButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(!EADCore.getInstance().checkPermissions(mInstance))
                    EADCore.getInstance().requestPermissions(mInstance);

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    Log.d(TAG, "RecordButton onClick ACTION DOWN");
                    Vibrator vibrator = (Vibrator) mInstance.getSystemService(mInstance.VIBRATOR_SERVICE);
                    vibrator.vibrate(200);
                    EADCore.getInstance().getWavParser().startRecording();
                    return false;
                }
                if(event.getAction()== MotionEvent.ACTION_UP){
                    Log.d(TAG, "RecordButton onClick ACTION UP");
                    EADCore.getInstance().getWavParser().stopRecording();
                    return true;
                }
                return false;
            }
        });



        //Start computation
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    startComputation();
            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(EADCore.getInstance().getWavParser().isPlaying()){
                    EADCore.getInstance().getWavParser().stopPlaying();
                    playButton.setImageDrawable(ContextCompat.getDrawable(mInstance, R.drawable.play));
                } else {
                    EADCore.getInstance().getWavParser().startPlaying();
                    playButton.setImageDrawable(ContextCompat.getDrawable(mInstance, R.drawable.stop));
                }
            }
        });

        browseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!EADCore.getInstance().checkPermissions(mInstance)){
                    EADCore.getInstance().requestPermissions(mInstance);
                } else {
                    showFileChooser();
                }
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(GlobalVariables.AUDIO_SOURCE_COMPLETE));
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            EADCore.getInstance().getWavParser().stopPlaying();
            playButton.setImageDrawable(ContextCompat.getDrawable(mInstance, R.drawable.play));
        }
    };

    private void startComputation(){
        Log.d(TAG, "startComputation");
        if(!isEditing){
            Log.d(TAG, "showing progress dialog");
            float displayProgress = ((float)(seekBar.getProgress()+50)/100);
            Toast.makeText(mInstance, "Starting time shift: " + displayProgress, Toast.LENGTH_SHORT).show();
            progressDialog.show();
            progressDialog.setProgress(0);
            isEditing=true;
            final float multiplier = displayProgress;
            Thread mThread = new Thread() {
                @Override
                public void run() {
                    EADCore.getInstance().getWavParser().reworkAudioFile(multiplier);
                }
            };
            mThread.start();
        } else {
            Toast.makeText(mInstance, "A process is already pending...", Toast.LENGTH_SHORT).show();
        }
    }

    public void updateComputation(int current, int total){
        //Log.d(TAG, "updateComputation"); Questa robaccia è più pesante della computazione in se. Levare
        if(progressDialog.isShowing()){
            progressDialog.setProgress((100*current)/total);
        }
    }

    public void endComputation(){
        progressDialog.dismiss();
        //Toast.makeText(this, "Computation done!" , Toast.LENGTH_SHORT).show();
        isEditing=false;
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    Log.d(TAG, "File Uri: " + uri.toString());

                    try{
                        EADCore.getInstance().setFilePath(this, uri);
                        //String path = EADCore.getInstance().getPath(this, uri);
                        //Log.d(TAG, "File Path: " + path);
                    } catch (Exception e){
                        e.printStackTrace();
                        Log.wtf(TAG, e);
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
